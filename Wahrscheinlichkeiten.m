% countRed( [ 'r' 'r' 'c' 'r' 'r' 'a' ] )

probabilities = zeros(7,1); % 1 = kein, 7 = 6
pulls = 100000;

for i = 1 : pulls
    ranMurmeln = MurmelZiehung();
    amount = countRed(ranMurmeln);
    probabilities( amount + 1) = probabilities( amount + 1 )  + 1;
end

fprintf("Wahrscheinlichkeiten: %f\n")
fprintf("keine rote Murmel: %f\n", probabilities(1) / pulls )
fprintf("eine rote Murmel: %f\n", probabilities(2) / pulls )
fprintf("zwei rote Murmeln: %f\n", probabilities(3) / pulls )
fprintf("drei rote Murmeln: %f\n", probabilities(4) / pulls )
fprintf("vier rote Murmeln: %f\n", probabilities(5) / pulls )
fprintf("fünf rote Murmeln: %f\n", probabilities(6) / pulls )
fprintf("sechs rote Murmeln: %f\n", probabilities(7) / pulls )

fprintf("checksum %f\n", sum( probabilities ) / pulls )

function [amount] = countRed(murmeln)
    amount = 0;
    for i = 1 : length(murmeln)
        if murmeln(i) == 'r'
            amount = amount +1;
        end
    end
end