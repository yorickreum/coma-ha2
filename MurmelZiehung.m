function[murmeln] = MurmelZiehung()

red = ['r' 'r' 'r' 'r' 'r' 'r'];
orange = ['o' 'o' 'o' 'o' 'o' 'o'];
yellow = ['y' 'y' 'y' 'y' 'y' 'y'];
green = ['g' 'g' 'g' 'g' 'g' 'g'];
blue = ['b' 'b' 'b' 'b' 'b' 'b'];
violett = ['v' 'v' 'v' 'v' 'v' 'v'];

murmeln = [red orange yellow green blue violett];
randomMurmeln = char( zeros(6,1) );

for i = 1:6
    while true % not nice, but there is no do-while-loop im matlab?
        r = randi(36);
        m = murmeln(r);
        if m ~= 't'
            break % end loop if murmel wasn't taken already
        end
    end
    randomMurmeln(i) = m;
    murmeln(r) = 't'; % mark murmel as taken
end

murmeln = sort(randomMurmeln).';

end