function [] = scalarproductCheck(A, B)
%SCALARPRODUCTCHECK Summary of this function goes here
%   Detailed explanation goes here

% Eingabe prüfen, B zur Not definieren

AB = A*B;

isSymetric = issymmetric(AB);
disp( isSymetric )
isPosDef = isposdef(AB); % or isposdefrec
disp( isPosDef )

% check functions 
% for i = 1 : 10000
%     r = rand(5); if( isposdef(r) == isposdefrec( r) )
%         fprintf('all fine');
%     else
%         fprintf('fuck'); disp( r );
%     end
% end

end

function [is] = isposdef(M)
    is = true;
    for i = 1 : size(M,1)
        H = det( M(1:i, 1:i) );
        if H <= 0
            is = false;
        end
    end
end

function [is] = isposdefrec(M, n)
    if(nargin < 2)
        n = 1;
    end
    is = true;
    if (n <= length(M) )
        H = det( M(1:n, 1:n) );
        if H <= 0
            is = false;
        else
            is = isposdefrec(M, n+1);
        end
    end
end
